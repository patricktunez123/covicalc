import { combineReducers } from "redux";
import { getCountries, getCountry } from "./countryReducers";
import { getContinents } from "./continentReducers";
import { getStates, getState } from "./stateReducers";

const reducers = combineReducers({
  Countries: getCountries,
  Country: getCountry,
  Continents: getContinents,
  States: getStates,
  State: getState,
});

export default reducers;
