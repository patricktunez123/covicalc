import { stateActionTypes } from "../../actionTypes";

const initialState = {
  loading: false,
  data: [],
  errorMessage: null,
};

/**
 * A reducer for getting all states and their infos
 * @author Patrick TUNEZERWANE
 * @since Mon, September 06, 2021
 */
export const getStates = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case stateActionTypes.GET_STATES_REQUEST:
      return {
        ...prevState,
        loading: true,
      };
    case stateActionTypes.GET_STATES_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };
    case stateActionTypes.GET_STATES_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

/**
 * A reducer for getting states by filter
 * @author Patrick TUNEZERWANE
 * @since Mon, September 06, 2021
 */
export const getState = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case stateActionTypes.GET_STATE_REQUEST:
      return {
        ...prevState,
        loading: true,
      };
    case stateActionTypes.GET_STATE_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };
    case stateActionTypes.GET_STATE_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
