import { stateActionTypes } from "../../actionTypes";
import request from "../../../helpers/api";

/**
 * A function to get all states and their stats
 * @author Patrick TUNEZERWANE
 * @since Mon, September 06, 2021
 */
export const getStates = () => async (dispatch) => {
  try {
    dispatch({
      type: stateActionTypes.GET_STATES_REQUEST,
    });

    const states = await request.get("/states?sort&yesterday");
    dispatch({
      type: stateActionTypes.GET_STATES_SUCCESS,
      payload: states?.data,
    });
  } catch (error) {
    dispatch({
      type: stateActionTypes.GET_STATES_FAIL,
      payload: error,
    });
  }
};

/**
 * A function to get state and its info
 * @author Patrick TUNEZERWANE
 * @since Mon, September 06, 2021
 */
export const getState = (stateName) => async (dispatch) => {
  try {
    dispatch({
      type: stateActionTypes.GET_STATE_REQUEST,
    });

    const state = await request.get(`/states/${stateName}?yesterday=true`);
    dispatch({
      type: stateActionTypes.GET_STATE_SUCCESS,
      payload: state?.data,
    });
  } catch (error) {
    dispatch({
      type: stateActionTypes.GET_STATE_FAIL,
      payload: error,
    });
  }
};
