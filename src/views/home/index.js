import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Spin, DatePicker, Button } from "antd";
import ContactMe from "../../components/ContactMe";
import Continents from "../../components/Continents";
import Me from "../../components/Me";
import { getStates, getState } from "../../redux/actions/stateActions";
import { getContinents } from "../../redux/actions/continentActions";
import { numberWithCommas } from "../../helpers/numbersFormatter";
import "./Home.scss";

const Home = () => {
  const dispatch = useDispatch();
  const [selectedState, setSelectedState] = useState();

  const { loading: statesLoading, data: states } = useSelector(
    (state) => state.States
  );

  const { loading: stateLoading, data: state } = useSelector(
    (state) => state.State
  );
  const { loading: continentsLoading, data: continents } = useSelector(
    (state) => state.Continents
  );

  const handleChange = (e) => {
    setSelectedState(e.target.value);
  };

  useEffect(() => {
    dispatch(getStates());
  }, [dispatch]);

  let stateName = null;
  if (selectedState === undefined) {
    stateName = "California";
  } else {
    stateName = selectedState;
  }

  useEffect(() => {
    dispatch(getState(stateName));
  }, [dispatch, stateName]);

  useEffect(() => {
    dispatch(getContinents());
  }, [dispatch]);

  return (
    <React.Fragment>
      <div className="covicalc--home">
        <div className="search--container">
          <h1 className="covicalc--title covicalc--text-white ">Updates</h1>
          <span className="covicalc--text covicalc--text-dark  covicalc--text-light covicalc--mb-1">
            Filter by state
          </span>
          <div className="covicalc--mb-2">
            {statesLoading ? (
              "Wait..."
            ) : (
              <div className="covicalc--search--input">
                <form>
                  <select
                    onChange={(e) => handleChange(e)}
                    data-live-search="true"
                    className="select--country"
                  >
                    {states?.map((state) => (
                      <option key={state?.state} data-tokens={state?.state}>
                        {state?.state}
                      </option>
                    ))}
                  </select>
                </form>
                <DatePicker className="small--devices" />
                <Button className="covicalc--secondary-btn small--devices">
                  Submit
                </Button>
              </div>
            )}
          </div>
        </div>

        <div className="country--stats-cards">
          <div className="card-top">
            <h1 className="covicalc--large--text covicalc--text-green covicalc--text-light covicalc--mb-1">
              {stateLoading ? (
                <Spin />
              ) : (
                state?.cases && numberWithCommas(state?.cases)
              )}
            </h1>
            {!stateLoading && (
              <span className="covicalc--text covicalc--text-white covicalc--text-bold">
                Cumulatively
              </span>
            )}
          </div>
          <div className="card-bottom">
            {stateLoading ? (
              <div className="wait--loader">
                <span>Wait...</span>
              </div>
            ) : (
              <>
                <div className="info">
                  <h1 className="covicalc--text-green covicalc--text-light covicalc--medium--text covicalc--mb-small">
                    {state?.testsPerOneMillion &&
                      numberWithCommas(state?.testsPerOneMillion)}
                  </h1>
                  <span className="covicalc--text-black covicalc--text-bold covicalc--text covicalc--mb-small">
                    Tests
                  </span>
                  <span className="text-muted covicalc--text-light covicalc--text">
                    {state?.tests && numberWithCommas(state?.tests)}
                  </span>
                </div>

                <div className="info">
                  <h1 className="covicalc--text-green covicalc--text-light covicalc--medium--text covicalc--mb-small">
                    {state?.active && numberWithCommas(state?.active)}
                  </h1>
                  <span className="covicalc--text-black covicalc--text-bold covicalc--text covicalc--mb-small">
                    Postive cases
                  </span>
                  <span className="text-muted covicalc--text-light covicalc--text">
                    {state?.cases && numberWithCommas(state?.cases)}
                  </span>
                </div>

                <div className="info">
                  <h1 className="covicalc--text-green covicalc--text-light covicalc--medium--text covicalc--mb-small">
                    {state?.activePerOneMillion
                      ? numberWithCommas(state?.activePerOneMillion)
                      : 0}
                  </h1>
                  <span className="covicalc--text-black covicalc--text-bold covicalc--text covicalc--mb-small">
                    Hospitalized
                  </span>
                  <span className="text-muted covicalc--text-light covicalc--text">
                    {state?.critical ? numberWithCommas(state?.critical) : 0}
                  </span>
                </div>

                <div className="info">
                  <h1 className="covicalc--text-green covicalc--text-light covicalc--medium--text covicalc--mb-small">
                    {state?.todayRecovered
                      ? numberWithCommas(state?.todayRecovered)
                      : 0}
                  </h1>
                  <span className="covicalc--text-black covicalc--text-bold covicalc--text covicalc--mb-small">
                    Recovered
                  </span>
                  <span className="text-muted covicalc--text-light covicalc--text">
                    {state?.recovered && numberWithCommas(state?.recovered)}
                  </span>
                </div>

                <div className="info">
                  <h1 className="covicalc--text-green covicalc--text-light covicalc--medium--text covicalc--mb-small">
                    {state?.todayDeaths && numberWithCommas(state?.todayDeaths)}
                  </h1>
                  <span className="covicalc--text-black covicalc--text-bold covicalc--text covicalc--mb-small">
                    Deaths
                  </span>
                  <span className="text-muted covicalc--text-light covicalc--text">
                    {state?.deaths && numberWithCommas(state?.deaths)}
                  </span>
                </div>

                <div className="info">
                  <h1 className="covicalc--text-green covicalc--text-light covicalc--medium--text covicalc--mb-small">
                    {state?.oneTestPerPeople
                      ? numberWithCommas(state?.oneTestPerPeople)
                      : 0}
                  </h1>
                  <span className="covicalc--text-black covicalc--text-bold covicalc--text covicalc--mb-small">
                    Vaccinated
                  </span>
                  <span className="text-muted covicalc--text-light covicalc--text">
                    {state?.testsPerOneMillion &&
                      numberWithCommas(state?.testsPerOneMillion)}
                  </span>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
      <Continents loading={continentsLoading} continents={continents} />
      <Me />
      <ContactMe />
    </React.Fragment>
  );
};

export default Home;
